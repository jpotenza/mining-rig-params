/**
 * Created by jpotenza on 4/10/18.
 */


module.exports = {
  models: [
    { brand: 'bitmain', model: 'S9', cards: '3', chipTotal: 189, watts: 1320, temp_keys: 'temp2_', description: 'Bitcoin Miner 13.5 TH/s', hashExpected: 13500, algo: 'sha-256', unit: 'GH/s' },
    { brand: 'bitmain', model: 'S9i', cards: '3', chipTotal: 189, watts: 1320, temp_keys: 'temp2_', description: 'Bitcoin Miner 14 TH/s', hashExpected: 14000, algo: 'sha-256', unit: 'GH/s' },
    { brand: 'bitmain', model: 'S9j', cards: '3', chipTotal: 189, watts: 1350, temp_keys: 'temp2_', description: 'Bitcoin Miner 14.5 TH/s', hashExpected: 14500, algo: 'sha-256', unit: 'GH/s' },
    { brand: 'bitmain', model: 'S15', cards: '4', chipTotal: 288, watts: 1596, temp_keys: 'temp2_', description: 'Bitcoin Miner 28 TH/s', hashExpected: 28000, algo: 'sha-256', unit: 'GH/s' },
    { brand: 'bitmain', model: 'S17', cards: '3', chipTotal: 144, watts: 2520, temp_keys: 'temp2_', description: 'Antminer S17 56 TH/s', hashExpected: 56000, algo: 'sha-256', unit: 'GH/s' },
    { brand: 'bitmain', model: 'S17 Pro', cards: '3', chipTotal: 144, watts: 2094, temp_keys: 'temp2_', description: 'Antminer S17 Pro 53 TH/s', hashExpected: 53000, algo: 'sha-256', unit: 'GH/s' },
    {
      brand: 'bitmain', model: 'A3', cards: '3', chipTotal: 180, watts:
        1275, temp_keys: 'temp2_', description: 'Siacoin Miner 815 GH/s', hashExpected: 815, algo: 'blake2b', unit: 'GH/s'
    },
    { brand: 'bitmain', model: 'B3', cards: '3', chipTotal: 180, watts: 380, temp_keys: 'temp2_', description: 'BTM Miner 780 H/s', hashExpected: 780, algo: 'tensority', unit: 'MH/s' },
    { brand: 'bitmain', model: 'D3', cards: '3', chipTotal: 180, watts: 1350, temp_keys: 'temp2_', description: 'DASH Miner 17 GH/s', hashExpected: 17000, algo: 'x11', unit: 'MH/s' },
    { brand: 'bitmain', model: 'E3', cards: '9', chipTotal: 18, watts: 800, temp_keys: 'temp2_', description: 'Ethereum Miner 190 MH/s', hashExpected: 190, algo: 'ethash', unit: 'MH/s' },
    { brand: 'bitmain', model: 'L3', cards: '4', chipTotal: 144, watts: 850, temp_keys: 'temp2_', description: 'Litecoin Miner 250 MH/s', hashExpected: 250, algo: 'scrypt', unit: 'MH/s' },
    { brand: 'bitmain', model: 'L3+', cards: '4', chipTotal: 288, watts: 1050, temp_keys: 'temp2_', description: 'Litecoin Miner 504 MH/s', hashExpected: 504, algo: 'scrypt', unit: 'MH/s' },
    { brand: 'bitmain', model: 'R4', cards: '2', chipTotal: 126, watts: 845, temp_keys: 'temp2_', description: 'Bitcoin Miner 8 TH/s', hashExpected: 8000, algo: 'sha-256', unit: 'TH/s' },
    { brand: 'bitmain', model: 'S7', cards: '3', chipTotal: 135, watts: 1293, temp_keys: 'temp', description: 'Bitcoin Miner 4.5 TH/s', hashExpected: 4500, algo: 'sha-256', unit: 'GH/s' },
    { brand: 'bitmain', model: 'T9', cards: '3', chipTotal: 171, watts: 1576, temp_keys: 'temp2_', description: 'Bitcoin Miner 12.5 TH/s', hashExpected: 12500, algo: 'sha-256', unit: 'GH/s' },
    { brand: 'bitmain', model: 'T9+', cards: '9', chipTotal: 162, watts: 1432, temp_keys: 'temp2_', description: 'Bitcoin Miner 12.5 TH/s', hashExpected: 12500, algo: 'sha-256', unit: 'GH/s' },
    { brand: 'bitmain', model: 'X3', cards: '3', chipTotal: 180, watts: 465, temp_keys: 'temp2_', description: 'Bitcoin X3 (220Kh)', hashExpected: 220, algo: 'cryptonight', unit: 'KH/s' },
    { brand: 'bitmain', model: 'V9', cards: '3', chipTotal: 135, watts: 1027, temp_keys: 'temp2_', description: 'Bitcoin Miner 4 TH/s', hashExpected: 4000, algo: 'sha-256', unit: 'GH/s' },
    { brand: 'bitmain', model: 'Z9-Mini', cards: '3', chipTotal: 12, watts: 300, temp_keys: 'temp2_', description: 'Z9 Mini Z Coin Miner 10k sol/s', hashExpected: 10, algo: 'equihash', unit: 'KSOL/s' },
    

    { brand: 'innosilicon', model: 'A9', cards: '', chipTotal: '', watts: 620, temp_keys: 'temp2_', description: 'Innosilicon A9 ZMaster (50ksol)', hashExpected: 50000, algo: 'equihash', unit: 'KSOL/s' },
    { brand: 'innosilicon', model: 'A10C', cards: '', chipTotal: '', watts: 850, temp_keys: 'temp2_', description: 'Innosilicon A10 ETHMaster (485Mh)', hashExpected: 485, algo: 'etHash', unit: 'MH/s' },
    { brand: 'innosilicon', model: 'A10B', cards: '', chipTotal: '', watts: 740, temp_keys: 'temp2_', description: 'Innosilicon A10 ETHMaster (432Mh)', hashExpected: 432, algo: 'etHash', unit: 'MH/s' },
    { brand: 'innosilicon', model: 'A10', cards: '', chipTotal: '', watts: 650, temp_keys: 'temp2_', description: 'Innosilicon A10 ETHMaster (365Mh)', hashExpected: 365, algo: 'etHash', unit: 'MH/s' },
    { brand: 'innosilicon', model: 'A8+', cards: '', chipTotal: '', watts: 480, temp_keys: 'temp2_', description: 'Innosilicon A8+ CryptoMaster (240kh)', hashExpected: 240000, algo: 'cryptonight', unit: 'KH/s' },
    { brand: 'innosilicon', model: 'A5+', cards: '', chipTotal: '', watts: 1500, temp_keys: 'temp2_', description: 'Innosilicon A5+ DashMaster (65Gh)', hashExpected: 65000, algo: 'x11', unit: 'GH/s' },
    { brand: 'innosilicon', model: 'D9+', cards: '', chipTotal: '', watts: 1230, temp_keys: 'temp2_', description: 'Innosilicon D9+ DecredMaster (2.8Th)', hashExpected: 2800, algo: 'blake256r14', unit: 'TH/s' },
    { brand: 'innosilicon', model: 'D9', cards: '', chipTotal: '', watts: 1000, temp_keys: 'temp2_', description: 'Innosilicon D9 DecredMaster (2.4Th)', hashExpected: 2400, algo: 'blake256r14', unit: 'TH/s' },
    { brand: 'innosilicon', model: 'A8', cards: '', chipTotal: '', watts: 350, temp_keys: 'temp2_', description: 'Innosilicon A8 CryptoMaster (160kh)', hashExpected: 160000, algo: 'cryptonight', unit: 'KH/s' },
    { brand: 'innosilicon', model: 'A5', cards: '', chipTotal: '', watts: 750, temp_keys: 'temp2_', description: 'Innosilicon A5 DashMaster (32.5Gh)', hashExpected: 325000, algo: 'x11', unit: 'GH/s' },
    { brand: 'innosilicon', model: 'A8C', cards: '', chipTotal: '', watts: 175, temp_keys: 'temp2_', description: 'Innosilicon A8C CryptoMaster (80kh)', hashExpected: 80000, algo: 'cryptonight', unit: 'KH/s' },
    { brand: 'innosilicon', model: 'S11', cards: '3', chipTotal: '135', watts: 1350, temp_keys: 'temp2_', description: 'Innosilicon S11 SiaMaster (4.3Th)', hashExpected: 4300, algo: 'blake2b', unit: 'TH/s' },
    { brand: 'innosilicon', model: 'T2T', cards: '', chipTotal: '', watts: 1980, temp_keys: 'temp2_', description: 'Innosilicon T2 Turbo (24Th)', hashExpected: 24000, algo: 'sha-256', unit: 'TH/s' },
    { brand: 'innosilicon', model: 'T2THF', cards: '', chipTotal: '', watts: 2200, temp_keys: 'temp2_', description: 'Innosilicon T2 Turbo+ (32Th)', hashExpected: 32000, algo: 'sha-256', unit: 'GH/s' },
    { brand: 'innosilicon', model: 'T2', cards: '', chipTotal: '', watts: 1570, temp_keys: 'temp2_', description: 'Innosilicon T2 Terminator (17.2Th)', hashExpected: 17200, algo: 'sha-256', unit: 'TH/s' },
    { brand: 'innosilicon', model: 'T3A', cards: '', chipTotal: '', watts: 2100, temp_keys: 'temp2_', description: 'Innosilicon T3 Terminator (43Th)', hashExpected: 43000, algo: 'sha-256', unit: 'GH/s' },
    { brand: 'innosilicon', model: 'A4+', cards: '', chipTotal: '', watts: 750, temp_keys: 'temp2_', description: 'Innosilicon A4+ LTCMaster (620Mh)', hashExpected: 620, algo: 'scrypt', unit: 'MH/s' },
    { brand: 'innosilicon', model: 'A6', cards: '', chipTotal: '', watts: 1500, temp_keys: 'temp2_', description: 'Innosilicon A6 LTCMaster (1.23Gh)', hashExpected: 123000, algo: 'scrypt', unit: 'GH/s' },
    { brand: 'innosilicon', model: 'A4', cards: '', chipTotal: '', watts: 1050, temp_keys: 'temp2_', description: 'Innosilicon A4 Dominator (280Mh)', hashExpected: 280, algo: 'scrypt', unit: 'MH/s' },
    { brand: 'innosilicon', model: 'B29', cards: '', chipTotal: '', watts: 900, temp_keys: 'temp2_', description: 'Halong Mining DragonMint B29 (2.1Th)', hashExpected: 2100, algo: 'blake256r14', unit: 'TH/s' },
    { brand: 'innosilicon', model: 'B52', cards: '', chipTotal: '', watts: 1380, temp_keys: 'temp2_', description: 'Halong Mining DragonMint B52 (3.83Th)', hashExpected: 3830, algo: 'blake2b', unit: 'TH/s' },
    { brand: 'innosilicon', model: 'T1', cards: '3', chipTotal: '189', watts: 1480, temp_keys: 'temp2_', description: 'Halong Mining DragonMint T1 (16Th)', hashExpected: 16000, algo: 'sha-256', unit: 'GH/s' },
    
    { brand: 'canaan', model: '821', cards: '4', chipTotal: 104, watts: 1200, temp_keys: 'temp2_', description: 'AvalonMiner 821 (11.5Th/s)', hashExpected: 11500, algo: 'sha-256', unit: 'GH/s' },
    { brand: 'canaan', model: '841', cards: '4', chipTotal: 104, watts: 1290, temp_keys: 'temp2_', description: 'AvalonMiner 841 (13.6Th/s)', hashExpected: 13600, algo: 'sha-256', unit: 'GH/s' },

  ]
}